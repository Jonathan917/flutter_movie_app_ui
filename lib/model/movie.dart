class Movie {
  int id;
  String title, public, duration, description, date, poster, detailImage;
  Rate starRate, metascrore;
  List<String> genre;
  List<Cast> cast;

  Movie({this.id, this.title, this.public, this.duration, this.description, this.date, this.poster, this.detailImage, this.starRate, this.metascrore, this.genre, this.cast});
}

class Rate {
  String score, reviews;

  Rate({this.score, this.reviews});
}

class Cast {
  String name, role, image;

  Cast({this.name, this.role, this.image});
}

List<Movie> movies = <Movie>[
  new Movie(
    id: 1,
    title: "Bloodshot",
    public: "PG 16",
    duration: "2h 12min",
    date: "2020",
    poster: "assets/images/poster_1.jpg",
    detailImage: "assets/images/backdrop_1.jpg",
    starRate: new Rate(score: "7.3", reviews: "150212"),
    metascrore: new Rate(score: "76", reviews: "50"),
    genre: ["Action", "Drama"],
    description: "American car designer Carroll Shelby and driver Kn Miles battle corporate interference and the laws of physics to build a revolutionary race car for Ford in order.",
    cast: [
      new Cast(name: "James Mangold", role: "Director", image: "assets/images/actor_1.png"),
      new Cast(name: "Matt Damo", role: "Carroll", image: "assets/images/actor_2.png"),
      new Cast(name: "Christian Bale", role: "Ken Miles", image: "assets/images/actor_3.png"),
      new Cast(name: "Caitriona Balfe", role: "Mollie", image: "assets/images/actor_4.png"),
    ],
  ),
  new Movie(
    id: 2,
    title: "Ford v Ferrari ",
    public: "PG 13",
    duration: "1h 50min",
    date: "2019",
    poster: "assets/images/poster_2.jpg",
    detailImage: "assets/images/backdrop_2.jpg",
    starRate: new Rate(score: "8.2", reviews: "239132"),
    metascrore: new Rate(score: "89", reviews: "132"),
    genre: ["Action", "Drama", "Biography"],
    description: "American car designer Carroll Shelby and driver Kn Miles battle corporate interference and the laws of physics to build a revolutionary race car for Ford in order.",
    cast: [
      new Cast(name: "James Mangold", role: "Director", image: "assets/images/actor_1.png"),
      new Cast(name: "Matt Damo", role: "Carroll", image: "assets/images/actor_2.png"),
      new Cast(name: "Christian Bale", role: "Ken Miles", image: "assets/images/actor_3.png"),
      new Cast(name: "Caitriona Balfe", role: "Mollie", image: "assets/images/actor_4.png"),
    ],
  ),
  new Movie(
    id: 3,
    title: "Onward",
    public: "PG 4",
    date: "2020",
    duration: "1h 42min",
    poster: "assets/images/poster_3.jpg",
    detailImage: "assets/images/backdrop_3.jpg",
    starRate: new Rate(score: "7.7", reviews: "7629"),
    metascrore: new Rate(score: "72", reviews: "34"),
    genre: ["Animation"],
    description: "American car designer Carroll Shelby and driver Kn Miles battle corporate interference and the laws of physics to build a revolutionary race car for Ford in order.",
    cast: [
      new Cast(name: "James Mangold", role: "Director", image: "assets/images/actor_1.png"),
      new Cast(name: "Matt Damo", role: "Carroll", image: "assets/images/actor_2.png"),
      new Cast(name: "Christian Bale", role: "Ken Miles", image: "assets/images/actor_3.png"),
      new Cast(name: "Caitriona Balfe", role: "Mollie", image: "assets/images/actor_4.png"),
    ],
  ),
];

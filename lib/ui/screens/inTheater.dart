import 'package:flutter/material.dart';
import 'package:flutter_movie_app_ui/model/movie.dart';
import 'package:flutter_movie_app_ui/ui/screens/movieDetail/movieDetail.dart';
import 'package:flutter_movie_app_ui/ui/widgets/genreSelection.dart';
import 'package:flutter_movie_app_ui/ui/widgets/movieCard.dart';

class MoviePresentation {
  String name;
  String imageUrl;
  double rate;

  MoviePresentation(String name, String imageUrl, double rate) {
    this.name = name;
    this.imageUrl = imageUrl;
    this.rate = rate;
  }
}

class InTheaterScreen extends StatefulWidget {
  @override
  _InTheaterScreenState createState() => _InTheaterScreenState();
}

class _InTheaterScreenState extends State<InTheaterScreen> {
  PageController _controller;
  int _initialPage = 1;
  int _currentPage;

  final List<MoviePresentation> listMovies = <MoviePresentation>[
    new MoviePresentation("BloodShot", "./assets/images/poster_1.jpg", 7.3),
    new MoviePresentation("Ford v Ferrari", "./assets/images/poster_2.jpg", 8.2),
    new MoviePresentation("Onward", "./assets/images/poster_3.jpg", 7.6)
  ];

  final List<String> genre = <String>["Action", "Crime", "Comedy", "Drama", "Humor", "Animation"];

  @override
  void initState() {
    super.initState();
    _controller = PageController(initialPage: _initialPage, viewportFraction: 0.8);
    _currentPage = _initialPage;
    // print(_controller.position.haveDimensions);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Padding(
              padding: const EdgeInsets.only(top: 30),
              child: GenreSelectionWidget(genre: genre),
            ),
          ),
          Expanded(
            child: PageView.builder(
              controller: _controller,
              itemCount: movies.length,
              onPageChanged: (value) {
                setState(() {
                  _currentPage = value;
                });
              },
              itemBuilder: (BuildContext context, int index) {
                final movie = movies[index];
                return GestureDetector(
                  onTap: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MovieDetailScreen(
                                  movie: movie,
                                )))
                  },
                  child: MovieCardWidget(
                    movie: movie,
                    pageController: _controller,
                    initialPage: _currentPage,
                    index: index,
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

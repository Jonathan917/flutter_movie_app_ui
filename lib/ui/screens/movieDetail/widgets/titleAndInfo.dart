import 'package:flutter/material.dart';
import 'package:flutter_movie_app_ui/model/movie.dart';

class TitleAndInfoWidget extends StatelessWidget {
  final Movie _movie;
  const TitleAndInfoWidget({
    @required Movie movie,
    Key key,
  })  : _movie = movie,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                _movie.title,
                style: TextStyle(fontSize: 27, fontWeight: FontWeight.w700),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    _movie.date,
                    style: TextStyle(fontSize: 16, color: Colors.grey[400], fontWeight: FontWeight.w700),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    _movie.public,
                    style: TextStyle(fontSize: 16, color: Colors.grey[400], fontWeight: FontWeight.w700),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    _movie.duration,
                    style: TextStyle(fontSize: 16, color: Colors.grey[400], fontWeight: FontWeight.w700),
                  ),
                ],
              )
            ],
          ),
          Container(
            height: 60,
            width: 60,
            decoration: BoxDecoration(color: Color.fromARGB(255, 253, 110, 142), borderRadius: BorderRadius.all(Radius.circular(18))),
            child: Icon(
              Icons.add,
              color: Colors.white,
              size: 25,
            ),
          )
        ],
      ),
    );
  }
}

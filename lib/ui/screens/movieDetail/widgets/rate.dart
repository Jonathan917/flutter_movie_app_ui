import 'package:flutter/material.dart';
import 'package:flutter_movie_app_ui/model/movie.dart';

class RateWidget extends StatelessWidget {
  final Movie _movie;
  const RateWidget({
    Key key,
    @required Movie movie,
  })  : _movie = movie,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 90,
      width: MediaQuery.of(context).size.width / 1.1,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(55), bottomLeft: Radius.circular(55)),
        boxShadow: [
          new BoxShadow(color: Colors.grey[300], blurRadius: 12.0, offset: Offset(-2, 5)),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              children: [
                Icon(
                  Icons.star,
                  color: Colors.yellow[700],
                  size: 33,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      _movie.starRate.score,
                      style: TextStyle(fontSize: 15, fontWeight: FontWeight.w700),
                    ),
                    Text(
                      "/10",
                      style: TextStyle(fontSize: 13),
                    )
                  ],
                ),
                Text(
                  _movie.starRate.reviews,
                  style: TextStyle(
                    fontSize: 13,
                    color: Colors.grey[400],
                    fontWeight: FontWeight.w700,
                  ),
                )
              ],
            ),
            Column(
              children: [
                Icon(
                  Icons.star_outline,
                  color: Colors.black,
                  size: 33,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Rate This",
                      style: TextStyle(fontSize: 15, fontWeight: FontWeight.w700),
                    ),
                  ],
                )
              ],
            ),
            Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 3, bottom: 4),
                  height: 26,
                  width: 30,
                  color: Colors.lightGreenAccent[700],
                  child: Center(
                    child: Text(
                      _movie.metascrore.score,
                      style: TextStyle(color: Colors.white, fontSize: 12),
                    ),
                  ),
                ),
                Text(
                  "Metascore",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  "${_movie.metascrore.reviews} reviews",
                  style: TextStyle(
                    fontSize: 13,
                    color: Colors.grey[400],
                    fontWeight: FontWeight.w700,
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

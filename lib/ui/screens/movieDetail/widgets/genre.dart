import 'package:flutter/material.dart';

class GenreWidget extends StatelessWidget {
  const GenreWidget({
    Key key,
    @required this.genre,
  }) : super(key: key);

  final List<String> genre;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
          children: genre
              .asMap()
              .map((key, value) => MapEntry(
                    key,
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: Chip(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        backgroundColor: Colors.white,
                        shape: StadiumBorder(side: BorderSide(color: Colors.grey[300])),
                        label: Text(
                          value,
                          style: TextStyle(fontSize: 15.0, color: Colors.grey[700], fontWeight: FontWeight.w700),
                        ),
                      ),
                    ),
                  ))
              .values
              .toList()),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_movie_app_ui/model/movie.dart';

class CastCardWidget extends StatelessWidget {
  final Cast _cast;
  const CastCardWidget({Key key, @required Cast cast})
      : _cast = cast,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 10, left: 10),
      width: 90,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CircleAvatar(
            backgroundImage: AssetImage(_cast.image),
            radius: 45,
          ),
          Text(
            _cast.name,
            maxLines: 2,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.w700),
          ),
          SizedBox(height: 5),
          Text(
            _cast.role,
            style: TextStyle(fontSize: 14, color: Colors.grey[500]),
          )
        ],
      ),
    );
  }
}

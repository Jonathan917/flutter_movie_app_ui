import 'package:flutter/material.dart';
import 'package:flutter_movie_app_ui/model/movie.dart';
import 'package:flutter_movie_app_ui/ui/screens/movieDetail/widgets/castCard.dart';
import 'package:flutter_movie_app_ui/ui/screens/movieDetail/widgets/genre.dart';
import 'package:flutter_movie_app_ui/ui/screens/movieDetail/widgets/rate.dart';
import 'package:flutter_movie_app_ui/ui/screens/movieDetail/widgets/titleAndInfo.dart';

class MovieDetailScreen extends StatelessWidget {
  final Movie _movie;
  final double headerSize = 340;

  const MovieDetailScreen({Key key, @required Movie movie})
      : _movie = movie,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [buildHeader(context), buildBody(context)],
      ),
    );
  }

  Container buildBody(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height - headerSize,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TitleAndInfoWidget(
                movie: _movie,
              ),
              SizedBox(height: 15),
              GenreWidget(genre: _movie.genre),
              SizedBox(height: 30),
              Text("Plot Summary", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700)),
              SizedBox(height: 20),
              Text(
                _movie.description,
                style: TextStyle(fontSize: 15, color: Colors.grey[600], height: 1.6),
              ),
              SizedBox(height: 50),
              Text("Cast & Crew", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700)),
              SizedBox(height: 10),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: _movie.cast
                      .asMap()
                      .map((key, value) => MapEntry(
                            key,
                            CastCardWidget(
                              cast: value,
                            ),
                          ))
                      .values
                      .toList(),
                ),
              ),
              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }

  Container buildHeader(BuildContext context) {
    return Container(
      height: this.headerSize,
      child: Stack(
        children: [
          Container(
            height: 300,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage(_movie.detailImage), fit: BoxFit.fill),
              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(35)),
            ),
          ),
          Positioned(
            top: 40,
            left: 15,
            child: IconButton(
              color: Colors.black,
              iconSize: 20,
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () => {Navigator.pop(context)},
            ),
          ),
          Positioned(
            right: 0,
            bottom: 0,
            child: RateWidget(movie: _movie),
          )
        ],
      ),
    );
  }
}

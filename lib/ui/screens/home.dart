import 'package:flutter/material.dart';
import 'package:flutter_movie_app_ui/ui/screens/inTheater.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with SingleTickerProviderStateMixin {
  final List<Tab> menu = <Tab>[
    Tab(
      text: 'In Theater',
    ),
    Tab(
      text: 'Box Office',
    ),
    Tab(
      text: 'Coming Soon',
    )
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: menu.length);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        leading: IconButton(
          color: Colors.black,
          icon: Icon(Icons.menu),
          onPressed: () => {},
        ),
        actions: [
          IconButton(
            color: Colors.black,
            icon: Icon(Icons.search),
            onPressed: () => {},
          )
        ],
        bottom: TabBar(
          controller: _tabController,
          labelPadding: EdgeInsets.only(top: 10, right: 20, left: 15),
          labelColor: Colors.black,
          unselectedLabelColor: Colors.black45,
          labelStyle: TextStyle(fontSize: 23, fontWeight: FontWeight.w700),
          tabs: menu,
          indicatorSize: TabBarIndicatorSize.label,
          indicator: UnderlineTabIndicator(borderSide: BorderSide(color: Colors.red, width: 4), insets: EdgeInsets.only(right: 75)),
          indicatorWeight: 5.0,
          isScrollable: true,
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: menu.map((Tab tab) {
          switch (tab.text) {
            case 'In Theater':
              return InTheaterScreen();
            default:
              return InTheaterScreen();
          }
        }).toList(),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class GenreSelectionWidget extends StatefulWidget {
  const GenreSelectionWidget({
    Key key,
    @required this.genre,
  }) : super(key: key);

  final List<String> genre;

  @override
  _GenreSelectionWidgetState createState() => _GenreSelectionWidgetState();
}

class _GenreSelectionWidgetState extends State<GenreSelectionWidget> {
  List<String> selectedGenre = <String>[];

  @override
  Widget build(BuildContext context) {
    return Row(
        children: widget.genre
            .asMap()
            .map((key, value) {
              return MapEntry(
                  key,
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: GestureDetector(
                      onTap: () => {
                        setState(() {
                          if (selectedGenre.contains(value)) {
                            selectedGenre.remove(value);
                          } else {
                            selectedGenre.add(value);
                          }
                        })
                      },
                      child: Chip(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        backgroundColor: selectedGenre.contains(value) ? Colors.red[400] : Colors.white,
                        shape: StadiumBorder(side: BorderSide(color: selectedGenre.contains(value) ? Colors.red[400] : Colors.grey[300])),
                        label: Text(
                          value,
                          style: TextStyle(fontSize: 13.0, color: selectedGenre.contains(value) ? Colors.white : Colors.grey[700], fontWeight: FontWeight.w700),
                        ),
                      ),
                    ),
                  ));
            })
            .values
            .toList());
  }
}

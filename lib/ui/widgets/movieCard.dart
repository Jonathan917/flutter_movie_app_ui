import 'package:flutter/material.dart';
import 'package:flutter_movie_app_ui/model/movie.dart';
import 'dart:math' as math;

class MovieCardWidget extends StatelessWidget {
  const MovieCardWidget({Key key, @required this.movie, @required this.pageController, @required this.index, @required this.initialPage}) : super(key: key);

  final Movie movie;
  final PageController pageController;
  final int index;
  final int initialPage;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: pageController,
      child: buildCardContent(context),
      builder: (context, child) {
        double value = 0;
        value = (index - initialPage).toDouble();
        value = (value * 0.038).clamp(-1, 1);
        return AnimatedOpacity(
          duration: Duration(milliseconds: 350),
          opacity: initialPage == index ? 1 : 0.4,
          child: Transform.rotate(
            origin: Offset(-250 * (index - initialPage).toDouble(), 0),
            angle: math.pi * value,
            child: child,
          ),
        );
      },
    );
  }

  Padding buildCardContent(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 25, left: 25, top: 50, bottom: 80),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage(movie.poster), fit: BoxFit.cover),
                borderRadius: BorderRadius.circular(40),
                boxShadow: [
                  new BoxShadow(color: Colors.black, blurRadius: 12.0, offset: Offset(-1, 5)),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, bottom: 10.0),
            child: Text(
              movie.title,
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.star,
                color: Colors.yellow[600],
                size: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(movie.starRate.score, style: TextStyle(fontSize: 13, fontWeight: FontWeight.w600, color: Colors.grey[700])),
              )
            ],
          )
        ],
      ),
    );
  }
}
